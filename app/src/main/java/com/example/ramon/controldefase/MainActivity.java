package com.example.ramon.controldefase;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;

import java.net.URI;
import java.net.URISyntaxException;

public class MainActivity extends AppCompatActivity
{
    private static final String TAG = "WebSocketClient";
    private TextView currentPercentageTv;
    private TextView love;
    private Button connectDisconnectButton;
    private SeekBar mSeekBar;
    private WebSocketClient webSocketClient;
    private Handler handler;

    private boolean keepSending = false;
    private int currentPercentage = 100;

    private Runnable sendPercentageValue = new Runnable()
    {
        @Override
        public void run()
        {
            if (webSocketClient.isOpen())
            {
                webSocketClient.send("a" + String.valueOf(currentPercentage));
                if (keepSending) handler.postDelayed(sendPercentageValue, 500);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        currentPercentageTv = findViewById(R.id.current_percentage);
        mSeekBar = findViewById(R.id.seek_bar);
        connectDisconnectButton = findViewById(R.id.button);
        love = findViewById(R.id.love);

        connectDisconnectButton.setOnLongClickListener(new View.OnLongClickListener()
        {
            @Override
            public boolean onLongClick(View view)
            {
                showHide();
                return true;
            }
        });

        mSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener()
        {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b)
            {
                Log.d("SeekBar", "i -> " + i + '\n' + "b -> " + b);
                currentPercentage = i;
                currentPercentageTv.setText(String.valueOf(i) + "%");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar)
            {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar)
            {

            }
        });
        mSeekBar.setVerticalScrollbarPosition(100);

        //Inicializar Handler
        handler = new Handler();
    }

    public void connectWebSocket(View v)
    {
        if (webSocketClient != null)
        {
            if (webSocketClient.isOpen())
            {
                webSocketClient.close();
            }
            else
            {
                initWebSocket();
            }
        }
        else
        {
            initWebSocket();
        }
    }

    private void initWebSocket()
    {
        URI uri;
        try
        {
            uri = new URI("ws://192.168.4.1/");
        }
        catch (URISyntaxException e)
        {
            e.printStackTrace();
            return;
        }

        webSocketClient = new WebSocketClient(uri)
        {
            @Override
            public void onOpen(ServerHandshake handshakedata)
            {
                Log.d(TAG, handshakedata.toString());
                keepSending = true;
                sendPercentageValue.run();

                runOnUiThread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        showStatusConnection(true);
                        connectDisconnectButton.setText("Desconectar");
                    }
                });
            }

            @Override
            public void onMessage(final String message)
            {
                Log.d(TAG, message);
            }

            @Override
            public void onClose(int code, String reason, boolean remote)
            {
                Log.d(TAG, reason);
                keepSending = false;
                runOnUiThread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        showStatusConnection(false);
                        connectDisconnectButton.setText("Conectar");
                    }
                });
            }

            @Override
            public void onError(Exception ex)
            {
                Log.d(TAG, ex.toString());
            }
        };

        Log.d(TAG, "Iniciando webSocketClient...");
        webSocketClient.connect();
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        if (webSocketClient.isOpen())
        {
            webSocketClient.close();
        }
    }

    private void showStatusConnection(boolean connected)
    {
        TextView statusConnection = findViewById(R.id.status_connection);

        if (connected)
        {
            statusConnection.setText("Conectado");
            statusConnection.setBackgroundColor(getResources().getColor(android.R.color
                    .holo_green_dark));
        }
        else
        {
            statusConnection.setText("Desconectado");
            statusConnection.setBackgroundColor(getResources().getColor(android.R.color
                    .holo_red_dark));
        }
    }

    private void showHide()
    {
        if (love.getVisibility() == View.GONE)
        {
            love.setVisibility(View.VISIBLE);
            currentPercentageTv.setVisibility(View.GONE);
            mSeekBar.setVisibility(View.GONE);
        }
        else
        {
            love.setVisibility(View.GONE);
            currentPercentageTv.setVisibility(View.VISIBLE);
            mSeekBar.setVisibility(View.VISIBLE);
        }
    }
}
